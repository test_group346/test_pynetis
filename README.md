
## Pynetis
This module is a request api library to old Netis router for now

## Downloading
It's only now available in gitlab
```bash
    git clone https://gitlab.com/test_group346/test_pynetis
```

# Usage

## Base example
```python    
from pynetis.router import Router

router = Router()

print(router.arp_list.value)
# [< ARPClient 'id': '1' | 'arp_ip': '192.168.1.1' | 'arp_mac': '40:B0:76:9D:23:58' >,
#  < ARPClient 'id': '2' | 'arp_ip': '192.168.0.2' | 'arp_mac': '98:28:A6:08:77:76' >, ...]
```

## What it's actually do ?

You can easily change option values or read data from router.
For example:
```python
from pynetis.values import Mode

router.dmz_enable.set(Mode.ENABLED)
```

All editable options bound with each other and for first view different options can be bounded, but it's not.
Example of bound options :

```python
router.wan_mode
router.repeater_ssid
```

This options for first view is not bound, but `repeater_ssid` only can be
changed , when `wan_mode` is `WANMode.WIRELESS`

If you want to change dependent option, you must firstly change main option.

```python
router.dmz_enable.set(Mode.DISABLED)
router.dmz_ip.set('192.168.0.85')
# Raises a ValueError, because dmz_enable is not enabled

router.dmz_enable.set(Mode.ENABLED)
router.dmz_ip.set('192.168.0.85')
```

All it's changes now in a local cache.
You can or discard them or apply them as well.
And before you set a new value or editing this option - option load state,
which was taken from router

For applying last group changes use `apply_last` and
for all - `apply_all`. Same with `cancel_last` and `cancel_all`.

```python
# post cache changes to a router.
# it's will clear local cache and update router object state
router.apply_last()
```

You also can do multiple changes and this changes will be applied.

```python
router.dmz_enalbe.set(Mode.ENABLED)
router.wan_mode.set(WANMode.WIRELESS)
router.apply_all()
```

## Editing list options

For add new item to list option, you must create a list item object.
For example, we want to add new virtual server to our router :

```python
from pynetis.options import VirtualServer
from pynetis.values import ServerProto

item = VirtualServer(
    vir_name='TEST1',
    vir_proto=ServerProto.UDP,
    vir_ip='192.168.0.84',
    vir_inport_end='10111',
    vir_inport_start='10111',
    vir_outport_end='10111',
    vir_outport_start='10111'
)

router.virtual_server_list.add_item(item)
router.apply_last()

print(router.virtual_server_list.value)
# [ < VirtualServer 'id': '1' | 'vir_name': 'TEST1' | 'vir_proto': '3'> ]
```

And we want to change this virtual server name
```python
router.virtual_server_list.change_item(item_id='1', vir_name='TEST2')
router.apply_last()
```

Finally, we want to delete this item
```python
#if you don't remember item id, you can find this item by fields
item = router.virtual_server_list.find(vir_name='TEST2')
router.virtual_server_list.pop_item(item_id=item.id)
router.apply_last()
```

### Readonly options

Readonly option is not bounded overall and only gives an option value:

```python
print(router.lan_mac.value)
# 40:B0:76:9D:23:58

router.lan_mac.set('51:A2:44:D0:55:12')
# ValueError : can't set attribute
```

# Roadmap

In future this module will be support new Netis routers
and will have some few features

