from __future__ import annotations

from typing import Union, List, TypeVar

_T = TypeVar("_T")


class Cache:

    """
    Using for cache any changes in router settings.
    """

    def __init__(self):
        self.groups = dict()
        self.last_updated_group: BoundGroup = None

    def create_group_from_name(self, option_name: str) -> BoundGroup:
        """
        Using for create a group from option name.
        If option already exists - ValueError
        :param option_name:
        :return:
        """

        if self.has(option_name):
            raise ValueError("Group already exists")

        prefix = BoundGroup.get_prefix(option_name)
        self.groups[prefix] = group = BoundGroup()
        return group

    def update_group(self, prefix: str, key: str, value: dict[str, str]) -> None:
        """
        Must updating throw this method, for track last changed group
        :param prefix: group prefix
        :param key: modified option name
        :param value: new option value
        :return:
        """
        group = self.get(bound_group_prefix=prefix)
        group.set(key, value)
        self.last_updated_group = group

    def has(self, bound_group_prefix: str) -> None:
        """
        Is group prefix in groups
        :param bound_group_prefix:
        :return:
        """
        return bound_group_prefix in self.groups

    def get(self, bound_group_prefix: str) -> BoundGroup:
        """
        If group is not exists, ValueError
        :param bound_group_prefix:
        :return:
        """
        if self.has(bound_group_prefix):
            return self.groups[bound_group_prefix]
        raise ValueError(f'No group with prefix {bound_group_prefix}')

    def find_by(self, option_name: str) -> Union[BoundGroup, None]:
        prefix = BoundGroup.get_prefix(option_name)
        if self.has(bound_group_prefix=prefix):
            return self.get(bound_group_prefix=prefix)

    def compress_all(self) -> List[dict[str, str]]:
        """
        Accumulate all groups data and returns it as list
        :return:
        """
        result = []
        for group in self.groups.values():
            if group:
                result.append(group.compress())
        return result


class BoundGroup:

    """
    Cache slot for bound options with each other
    and get an all options json data together
    """

    def __init__(self):
        self.buffer = dict()
        self.options: list[_T] = []
        self.last_updated: str = None

    def get_option_by_name(self, option_name: str) -> _T:
        """
        Get option from group by this name
        :param option_name:
        :return:
        """
        filtered = filter(lambda option: option.name == option_name, self.options)
        return list(filtered)[0]

    def set(self, key, value) -> None:
        """
        Modify option data in this cache slot
        and track last changed option
        :param key: modified option name
        :param value: new option value
        :return:
        """
        self.buffer[key] = value
        self.last_updated = key

    def compress_buffer(self) -> dict[str, str]:
        """
        Returns only options, which was changed
        :return:
        """
        result = dict()
        for option_json in self.buffer.values():
            result.update(option_json)
        return result

    def compensate_lack(self, lack_options: list[str]) -> dict[str, str]:
        """
        Returns options data, which is not modified yet,
        but necessary for post with other bounded options
        :param lack_options:
        :return:
        """
        filtered = filter(lambda option_: option_.name in lack_options, self.options)
        result = dict()
        for option in filtered:
            result.update(option.to_json())
        return result

    def compress(self) -> dict[str, str]:
        """
        Returns a not modified options data and changed options data together
        :return:
        """
        buffer = self.compress_buffer()
        buffer_keys = set(buffer.keys())
        options_names = set(option.name for option in self.options)
        lack_options = options_names.difference(buffer_keys)

        buffer.update(self.compensate_lack(lack_options=lack_options))
        return buffer

    @staticmethod
    def get_prefix(option_name: str) -> str:
        """
        Using for get a prefix from option name.
        Prefix works such as id
        :param option_name:
        :return:
        """
        try:
            prefix, _ = option_name.split('_', maxsplit=1)
        except ValueError:
            prefix = option_name
        return prefix

    def __bool__(self):
        return bool(self.buffer)
