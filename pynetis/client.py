from requests import Response, Session


def to_data_string(dictionary: dict) -> str:
    """
    netis router only get a key=value&key2=value2 data format
    :param dictionary:
    :return:
    """
    return "&".join(f"{key}={value}" for key, value in dictionary.items())


class Client:

    GET_FIELDS = {
        "mode_name": 'netcore_get',
        'no': 'no'
    }

    POST_FIELDS = {
        "mode_name": "netcore_set"
    }

    __slots__ = (
        "username",
        "password",
        "host_url",
        "_session"
    )

    def __init__(
            self,
            username: str = None,
            password: str = None,
            host_url: str = 'http://192.168.1.1',
    ):
        self.username = username
        self.password = password
        self.host_url = host_url
        self._session = Session()
        self._session.auth = (username, password)

    def _request(self, method: str, url: str, data: dict, **kwargs) -> Response:
        request_function = getattr(self._session, method)
        return request_function(url=url, data=to_data_string(data), **kwargs)

    def get_data(self) -> Response:
        """
        Using only for fetch data from router
        :return:
        """
        url = self.host_url + '/cgi-bin-igd/netcore_get.cgi'
        return self._request('get', url, self.GET_FIELDS)

    def post(self, data: dict) -> Response:
        """
        Using for post data to router
        :param data: json data with only string keys and values
        :return:
        """
        url = self.host_url + '/cgi-bin-igd/netcore_set.cgi'
        return self._request(
            'post',
            url=url,
            data={**self.POST_FIELDS, **data},
            headers={'Content-Type': 'text/plain'}
        )

    def close(self) -> None:
        self._session.close()
