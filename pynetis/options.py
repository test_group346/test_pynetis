from __future__ import annotations

from abc import ABC, abstractmethod
from copy import copy
from ipaddress import IPv4Address
from typing import Callable, List, Type, TypeVar, Sequence, Protocol, Union, Tuple, Any
from .cache import Cache, BoundGroup


class Router(Protocol):
    """
    Represents a router
    """
    _cache: Cache


_T = TypeVar('_T')


def get_additional_json(option_name: str) -> dict[str, str]:
    """
    If `additional_fields` not passed and `use_default_additional` is True,
    it's generate a default additional fields from option_name
    :param option_name:
    :return:
    """
    prefix = BoundGroup.get_prefix(option_name)
    return {prefix + "_set": "1"}


def copy_list(ls: list[_T]) -> list[_T]:
    """
    Uses only for list options to copy all objects in list
    :param ls:
    :return:
    """
    new_list = []
    for obj in ls:
        new_list.append(copy(obj))
    return new_list


class Option(ABC):
    """
    Base representation of option
    """

    def __init__(
            self,
            name: str = None,
            value: _T = None,
            access_in: Comparator = None,
            validator: Callable[[_T], bool] = None,
            bound_with: str = None,
            additional_fields: dict[str, str] = None,
            use_default_additional: bool = False,
            dependent_fields: dict[str, dict] = None,
    ):
        """
        :param name: option name in router
        :param value: option default value
        :param access_in: if `access_in` returns a True, this option can be changed, else - ValueError
        :param validator: validate a new option value
        :param bound_with: if `bound_with` passed, bounds with another cache group, and not create own
        :param additional_fields: specific fields, which must be passed to router
        :param use_default_additional: using for generate `additional_fields`
        """
        default_validator = lambda val: True
        _addition_fields = get_additional_json(name) if use_default_additional else {}
        self._bound_with = bound_with
        self._access_in = access_in or NullComparator('')
        self._validator = validator or default_validator
        self._router: Router = None
        self.dependent_fields = dependent_fields or dict()
        self.additional_fields = additional_fields or _addition_fields
        self.name = name
        self._value = value

    def validate(self, value: _T) -> None:
        valid = self._validator(value)
        if not valid:
            raise ValueError(f'Value "{value}" is not valid for "{self.name}"')

    def accessibility(self) -> bool:
        """
        Is this option can be changed or not
        :return:
        """
        return self._access_in.compare(self._router)

    @property
    def value(self) -> _T:
        return self._value

    @value.setter
    def value(self, val: _T) -> None:
        """
        If value is inaccessible raise ValueError
        :param val:
        :return:
        """
        self.validate(val)

        if not self.accessibility():
            value_ = self._access_in.expected_value
            name_ = self._access_in.router_option_name
            raise ValueError(f'Cannot change option, because dependent'
                             f' option "{name_}" haven\'t value `{value_}`')

        self._value = val

    def __repr__(self):
        return f"< {self.__class__.__name__} | '{self.name}'>"


class EditableOptionContext(Option):

    def to_json(self) -> dict[str, str]:
        """
        Using for a post necessary data to cache and then to router
        :return:
        """
        result = self.additional_fields.copy()

        dependent_fields = None
        if isinstance(self.value, str):
            dependent_fields = self.dependent_fields.get()

        if dependent_fields is None:
            dependent_fields = self.dependent_fields.get('default', dict())

        result.update(dependent_fields)

        return result

    @property
    def group_prefix(self) -> str:
        """
        Returns a group prefix based on option name.
        If `bound_with` is passed based on this
        :return:
        """
        return BoundGroup.get_prefix(self._bound_with or self.name)

    def access_option(self) -> Union[Tuple[Type[EditableOptionContext], Any], None]:
        """
        If option have `access_in` attribute, returns access option object and value
        :return:
        """

        if isinstance(self._access_in, NullComparator):
            return

        group = self._router._cache.get(self.group_prefix)
        option = group.get_option_by_name(self._access_in.real_option_name)
        return option, self._access_in.expected_value

    def safe_set(self, value: _T) -> None:
        """
        Automatically set access option value to expected
        :param value:
        :return:
        """

        access_values = self.access_option()
        if access_values is not None and not self.accessibility():
            access_option, expected_value = access_values
            access_option.safe_set(expected_value)

        self.set(value)

    def to_cache(self, data: dict[str, str]) -> None:
        """
        Sends to cache changed option data
        :param data:
        :return:
        """
        cache = self._router._cache
        cache.update_group(
            prefix=self.group_prefix,
            key=self.name,
            value=data
        )

    def set(self, value) -> None:
        """
        Send changes to cache on new value
        :param value:
        :return:
        """
        self.value = value
        self.to_cache(self.to_json())

    def _bound(self) -> None:
        """
        Bound this option with group for a post changes to router
        with bounded options
        :return:
        """
        cache = self._router._cache
        group_option_name = self._bound_with or self.name
        group = cache.find_by(group_option_name)

        if group is None:
            group = cache.create_group_from_name(group_option_name)

        group.options.append(self)


class SingleOptionContext(Option):
    """
    Represents option, which have a single value
    """

    def __init__(self, without_load: bool = False, *args, **kwargs):
        super(SingleOptionContext, self).__init__(*args, **kwargs)
        self.without_load = without_load

    def from_json(self, data: dict) -> None:
        """
        Pops a option value from `data`.
        If option name not in `data` - raises KeyError
        :param data:
        :return:
        """
        if not self.without_load:
            value = data.pop(self.name)
            self._value = value

    def __repr__(self):
        return f"< SingleOption '{self.name}' : '{self.value}' >"


class Cancelable(SingleOptionContext, EditableOptionContext):
    _load_state = None

    def cancel_changes(self) -> None:
        """
        Delete change data from cache and return load state for this option
        :return:
        """
        group = self._router._cache.get(self.group_prefix)

        try:
            group.buffer.pop(self.name)
        except KeyError:
            pass

        self._value = copy(self._load_state)

    def set(self, value) -> None:
        self.cancel_changes()
        super(Cancelable, self).set(value)


class SingleOption(Cancelable):
    """
    Editable single option representation
    """

    def from_json(self, data: dict) -> None:
        super(SingleOption, self).from_json(data)
        self._load_state = copy(self._value)

    def to_json(self) -> dict[str, str]:
        json_ = super(SingleOption, self).to_json()
        json_.update({self.name: self.value})
        return json_


class ReadOnlySingleOption(SingleOptionContext):
    """
    Represents a readonly option
    """

    @property
    def value(self) -> _T:
        return self._value

    def accessibility(self) -> bool:
        return False


class ListItem:
    """
    Represents an item, which in list option
    """

    def to_json(self, increment_id=False) -> dict[str, str]:
        """
        Returns all fields, but with id in end of key
        and also changes id, if necessary
        :param increment_id:
        :return:
        """
        result_dict = self.__dict__.copy()
        changed_id = self._get_id_state(increment_id=increment_id)

        result_dict.pop('id')
        result_dict = self._with_id(result_dict)

        result_dict.update(changed_id)

        return result_dict

    def _with_id(self, json_: dict[str, str]) -> dict[str, str]:
        """
        Returns fields with id in end
        :param json_:
        :return:
        """
        new_dict = dict()
        for key in json_.keys():
            with_id = key + str(self.id)
            new_dict[with_id] = json_[key]
        return new_dict

    def _get_id_state(self, increment_id: bool) -> dict[str, str]:
        """
        Returns json with modified or not id field
        :param increment_id:
        :return:
        """
        id_ = int(self.id)
        operational_id = id_

        if increment_id:
            operational_id += 1

        return {f"id{id_}": operational_id}

    @property
    def repr_values(self) -> list[str]:
        """
        Returns all values in view 'key : value'
        :return:
        """
        items = self.__dict__.items()
        return [f"'{key}': '{value}'" for key, value in items]

    def get_repr_values(self) -> list[str]:
        """
        Using for modify , how many or exactly which fields must be in __repr__
        :return:
        """
        return self.repr_values[:3]

    def __repr__(self):
        obj_name = f"< {self.__class__.__name__} "
        obj_values = " | ".join(self.get_repr_values())
        return obj_name + obj_values + " >"


class ListOptionContext(SingleOptionContext):

    def __init__(
            self,
            list_item_class: Type[ListItem],
            item_validator: Callable[[Union[dict, Type[Option]]], bool] = None,
            **kwargs
    ):
        """
        :param list_item_class: list item wrapper class
        :param item_validator: using for validate item on add or load
        :param kwargs:
        """
        default_item_validator = lambda item: True
        self.list_item_class = list_item_class
        self.item_validator = item_validator or default_item_validator
        super(ListOptionContext, self).__init__(**kwargs)

    def find(self, **filter_fields) -> Union[_T, None]:
        """
        Try to find item by filtering item fields
        :param filter_fields:
        :return:
        """

        def item_have_fields(item_: _T) -> bool:
            expect = []
            for field_name in filter_fields:
                value = getattr(item_, field_name, None)
                expect.append(value == filter_fields[field_name])
            return all(expect)

        for item in self.value:
            if item_have_fields(item):
                return item

    def from_json(self, data: dict) -> None:
        value = data.pop(self.name)
        self._value = self.transform_objects(value)

    def validate(self, value: _T) -> None:
        super(ListOptionContext, self).validate(value)
        if not issubclass(type(value), Sequence):
            raise TypeError("Incorrect type")

    def transform_objects(self, objects: list[dict]) -> List[_T]:
        """
        Transform json objects to a class objects
        :param objects:
        :return:
        """
        transformed = []

        for obj in objects:
            list_item = self.list_item_class(**obj)
            transformed.append(list_item)

        return transformed

    def __repr__(self):
        return f"< ListOption name : {self.name} | items : {len(self.value) if self.value else 0}>"


class ReadOnlyListOption(ListOptionContext):
    """
    Represents a readonly list options.
    Not inheritance from ReadOnlySingleOption due type conflict
    """

    @property
    def value(self) -> _T:
        return self._value

    def accessibility(self) -> bool:
        return False


class ListOption(Cancelable, ListOptionContext):

    def from_json(self, data: dict) -> None:
        super(ListOption, self).from_json(data)
        self._load_state = copy(self._value)

    def to_json(self) -> dict[str, str]:
        json_ = super(ListOption, self).to_json()
        for item in self.value:
            json_.update(item.to_json())
        return json_

    def change_item(self, item_id: Union[str, int], **item_fields) -> None:
        """
        Changes item, if item in this list.
        If item not found - raise ValueError
        :param item_id:
        :param item_fields:
        :return:
        """
        self.cancel_changes()

        item = self.find(id=str(item_id))
        if not item:
            raise ValueError(f"Item with id `{item_id}` not found")

        for field_name in item_fields:
            setattr(item, field_name, item_fields[field_name])

        self.to_cache(self.to_json())

    def add_item(self, item: _T) -> None:
        """
        Adds an item to list option and send data to cache
        :param item:
        :return:
        """
        self.cancel_changes()

        new_list = copy_list([item] + self.value)

        for index, item_ in enumerate(new_list):
            item_.id = str(index + 1)

        self._value = new_list
        self.to_cache(self.to_json())

    def pop_item(self, item_id: str | int) -> _T:
        """
        Pops item and send data to cache
        :param item_id:
        :return:
        """
        self.cancel_changes()

        item_index = int(item_id) - 1
        popped_item = self.value[item_index]
        before = self.value[:item_index]
        after = self.value[item_index + 1:]
        new_list = copy_list(before + after)
        data = dict()

        for index, item in enumerate(new_list):
            do_increase = item in after
            item.id = str(index + 1)
            item_data = item.to_json(increment_id=do_increase)

            data.update(item_data)

        self._value = new_list
        self.to_cache(data)

        return popped_item


class ComparatorContext(ABC):

    """
    Represents a comparator
    """

    def __init__(
            self, router_option_name: str,
            real_option_name: str = None
    ):
        self.router_option_name = router_option_name
        self.real_option_name = real_option_name or router_option_name

    @abstractmethod
    def compare(self, router: Router) -> bool:
        ...


class Comparator(ComparatorContext):
    """
    Using for compare expected option value with real
    """

    def __init__(
            self,
            expected_value: str,
            *args, **kwargs
    ):
        super(Comparator, self).__init__(*args, **kwargs)
        self.expected_value = expected_value

    def compare(self, router: Router) -> bool:
        option_object = getattr(router, self.router_option_name)
        return option_object.value == self.expected_value


class ListComparator(ComparatorContext):

    """
    Option value must be in expected values list.
    Takes first expected value to `safe_set`,
     if `preferred_value_index` is not passed
    """

    def __init__(
            self,
            expected_values: str,
            preferred_value_index: int = None,
            *args, **kwargs
    ):
        super(ListComparator, self).__init__(*args, **kwargs)
        self.expected_value = expected_values[preferred_value_index or 0]
        self._list = expected_values

    def compare(self, router: Router) -> bool:
        option_object = getattr(router, self.router_option_name)
        return option_object.value in self._list


class ValidationComparator(ComparatorContext):

    """
    Depends on option validator.
    If option value is not valid - raises ValueError
    """

    def compare(self, router: Router) -> bool:
        option_object = getattr(router, self.router_option_name)
        option_value = option_object.value
        option_object.validate(option_value)
        return True


class NullComparator(ComparatorContext):

    def compare(self, router: Router) -> bool:
        return True


valid_ip = lambda ip: IPv4Address(ip) is not None


class VirtualServerListItem(ListItem):

    def __init__(
            self,
            vir_name: str,
            vir_ip: str,
            vir_proto: str,
            vir_outport_start: str,
            vir_outport_end: str,
            vir_inport_start: str,
            vir_inport_end: str,
            id: str = None,
    ):
        self.id = id or 0
        self.vir_name = vir_name
        self.vir_ip = vir_ip
        self.vir_proto = vir_proto
        self.vir_outport_start = vir_outport_start
        self.vir_outport_end = vir_outport_end
        self.vir_inport_start = vir_inport_start
        self.vir_inport_end = vir_inport_end


class ARPListItem(ListItem):

    def __init__(
            self,
            arp_ip: str,
            arp_mac: str,
            arp_st: str,
            arp_dev: str,
            id: str = None
    ):
        self.id = id or 0
        self.arp_ip = arp_ip
        self.arp_mac = arp_mac
        self.arp_st = arp_st
        self.arp_dev = arp_dev


class DHCPClientListItem(ListItem):

    def __init__(
            self,
            ip: str,
            mac: str,
            host: str,
            reserved: str,
            status: str,
            id: str = None
    ):
        self.id = id or 0
        self.ip = ip
        self.mac = mac
        self.host = host
        self.reserved = reserved
        self.status = status

    def get_repr_values(self) -> list[str]:
        return self.repr_values[1:4]


class ReservationListItem(ListItem):

    def __init__(
            self,
            reserve_des: str,
            reserve_ip: str,
            reserve_mac: str,
            id: str = None,
    ):
        self.id = id or 0
        self.reserve_des = reserve_des
        self.reserve_ip = reserve_ip
        self.reserve_mac = reserve_mac
