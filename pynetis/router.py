import json

import requests.exceptions
from requests import Response

from .client import Client
from .values import *
from .options import *


class Router(Client):
    """
    Represents an old mark netis router
    """

    def __init__(self, **kwargs):
        super(Router, self).__init__(**kwargs)
        self._cache = Cache()

        self._single_options = []
        self._list_options = []

        self.add_options()
        self._init_options()
        self.__validate_account()

    def __validate_account(self) -> None:
        try:
            self.update()
        except requests.exceptions.ContentDecodingError:
            raise ConnectionError("Username or password is invalid")

    # noinspection PyAttributeOutsideInit
    def add_options(self) -> None:
        """
        Just adds all options to router object
        :return:
        """

        self.dmz_enable = SingleOption(
            name='dmz_enable',
            validator=lambda value: Mode(str(value)),
            access_in=Comparator('super_dmz_enable', Mode.DISABLED),
            use_default_additional=True
        )
        self.dmz_ip = SingleOption(
            name='dmz_ip',
            access_in=Comparator('dmz_enable', Mode.ENABLED),
            validator=valid_ip
        )

        self.super_dmz_enable = SingleOption(
            name='super_dmz_enable',
            access_in=Comparator('dmz_enable', Mode.DISABLED),
            validator=lambda value: Mode(str(value)),
        )
        self.super_dmz_address = SingleOption(
            name='super_dmz_addr',
            access_in=Comparator('super_dmz_enable', Mode.ENABLED)
        )

        self.arp_list = ReadOnlyListOption(
            name='arp_list',
            list_item_class=ARPListItem
        )

        self.virtual_server_list = ListOption(
            name='virtual_server_list',
            list_item_class=VirtualServerListItem,
            additional_fields={'vir_server_set': '1'}
        )

        self.wan_mode = SingleOption(
            name='access_mode',
            validator=lambda mode: WANMode(str(mode)),
            additional_fields={'wan_set': '1'}
        )

        # wireless options
        wan_wireless_comparator = Comparator(
            router_option_name='wan_mode',
            real_option_name='access_mode',
            expected_value=WANMode.WIRELESS
        )
        self.repeater_ssid = SingleOption(
            name='repeater_ssid',
            access_in=wan_wireless_comparator,
            bound_with='access_mode'
        )
        self.repeater_security_type = SingleOption(
            name='rp_sec_mode',
            access_in=wan_wireless_comparator,
            bound_with='access_mode',
            validator=lambda type_: SecurityType(str(type_))
        )

        repeater_comparator = ListComparator(
            router_option_name='repeater_security_type',
            real_option_name='rp_sec_mode',
            expected_values=[SecurityType.WPA_PSK, SecurityType.WPA2_PSK],
            preferred_value_index=1,
        )
        self.repeater_key_type = SingleOption(
            name='rp_key_type',
            access_in=repeater_comparator,
            bound_with='access_mode',
            validator=lambda type_: KeyType(str(type_))
        )
        self.repeater_key_mode_wpa = SingleOption(
            name='rp_key_mode_wpa',
            access_in=repeater_comparator,
            bound_with='access_mode',
            validator=lambda mode: KeyModeWPA(str(mode))
        )
        self.repeater_key_wpa = SingleOption(
            name='rp_key_wpa',
            access_in=repeater_comparator,
            bound_with='access_mode'
        )

        self.wan_connection_type = SingleOption(
            name='conntype',
            access_in=wan_wireless_comparator,
            bound_with='access_mode',
            validator=lambda type_: ConnectionType(str(type_))
        )

        # static ip options
        static_comparator = Comparator(
            router_option_name='wan_connection_type',
            real_option_name='conntype',
            expected_value=ConnectionType.STATIC_IP
        )
        self.static_wan_ip = SingleOption(
            name='wan_ip',
            access_in=static_comparator,
            validator=valid_ip,
            bound_with='access_mode'
        )
        self.static_wan_mask = SingleOption(
            name='wan_mask',
            access_in=ValidationComparator('static_wan_ip'),
            bound_with='access_mode'
        )
        self.static_wan_gateway = SingleOption(
            name='wan_gw',
            access_in=ValidationComparator('static_wan_mask'),
            validator=valid_ip,
            bound_with='access_mode',
        )

        # dns servers
        dns_comparator = ListComparator(
            router_option_name='wan_connection_type',
            real_option_name='conntype',
            expected_values=[
                ConnectionType.PPPOE,
                ConnectionType.L2TP,
                ConnectionType.PPTP,
                ConnectionType.MAXIS_PPPOE,
                ConnectionType.UNIFI_PPPOE,
                ConnectionType.STATIC_IP,
            ]
        )
        self.static_primary_dns = SingleOption(
            name='dns_a',
            access_in=dns_comparator,
            validator=valid_ip,
            bound_with='access_mode',
        )
        self.static_secondary_dns = SingleOption(
            name='dns_b',
            access_in=dns_comparator,
            validator=valid_ip,
            bound_with='access_mode',
        )

        # MAC
        mac_comparator = ListComparator(
            router_option_name='wan_connection_type',
            real_option_name='conntype',
            expected_values=[
                ConnectionType.PPPOE,
                ConnectionType.UNIFI_PPPOE,
                ConnectionType.MAXIS_PPPOE,
                ConnectionType.PPTP,
                ConnectionType.L2TP,
            ]
        )
        self.mac_address = SingleOption(
            name="mac_addr",
            access_in=mac_comparator,
            bound_with='access_mode',
        )

        # PPPOE
        pppoe_comparator = ListComparator(
            router_option_name='wan_connection_type',
            real_option_name='conntype',
            expected_values=[
                ConnectionType.PPPOE,
                ConnectionType.MAXIS_PPPOE,
                ConnectionType.UNIFI_PPPOE
            ]
        )
        self.pppoe_pwd = SingleOption(
            name='pppoe_pwd',
            access_in=pppoe_comparator,
            bound_with='access_mode',
        )
        self.pppoe_username = SingleOption(
            name='pppoe_username',
            access_in=pppoe_comparator,
            bound_with='access_mode'
        )
        self.pppoe_connect_mode = SingleOption(
            name='ppp_connect_mode',
            access_in=pppoe_comparator,
            bound_with='access_mode',
            validator=lambda mode: ConnectTypeMode(str(mode))
        )
        self.pppoe_demand_time = SingleOption(
            name='ppp_time',
            access_in=Comparator('pppoe_connect_mode', ConnectTypeMode.ON_DEMAND),
            bound_with='access_mode'
        )
        self.pppoe_mtu = SingleOption(
            name="pppoe_mtu",
            access_in=pppoe_comparator,
            bound_with='access_mode',
        )
        self.pppoe_server_name = SingleOption(
            name='pppoe_service_name',
            access_in=pppoe_comparator,
            bound_with='access_mode'
        )
        self.pppoe_ac_name = SingleOption(
            name='pppoe_ac_name',
            access_in=pppoe_comparator,
            bound_with='access_mode'
        )

        # PPTP
        pptp_comparator = Comparator(
            router_option_name="wan_connection_type",
            real_option_name='conntype',
            expected_value=ConnectionType.PPTP
        )
        self.pptp_username = SingleOption(
            name='pptp_username',
            access_in=pptp_comparator,
            bound_with='access_mode'
        )
        self.pptp_pwd = SingleOption(
            name='pptp_pwd',
            access_in=pptp_comparator,
            bound_with='access_mode'
        )
        self.pptp_server_ip = SingleOption(
            name='pptp_server_ip',
            access_in=pptp_comparator,
            bound_with='access_mode'
        )
        self.pptp_server_name = SingleOption(
            name='pptp_server_name',
            access_in=pptp_comparator,
            bound_with='access_mode'
        )
        self.pptp_type = SingleOption(
            name='pptp_type',
            access_in=pptp_comparator,
            bound_with='access_mode',
            validator=lambda type_: ConnectionIPType(str(type_)),
            without_load=True,
            value=ConnectionIPType.DYNAMIC_IP
        )
        self.pptp_connect_mode = SingleOption(
            name='pptp_connect_mode',
            access_in=pptp_comparator,
            bound_with='access_mode',
            validator=lambda mode: ConnectTypeMode(str(mode))
        )
        self.pptp_demand_time = SingleOption(
            name='pptp_time',
            access_in=Comparator('pptp_connect_mode', ConnectTypeMode.ON_DEMAND),
            bound_with='access_mode'
        )
        self.pptp_mtu = SingleOption(
            name="pptp_mtu",
            access_in=pptp_comparator,
            bound_with='access_mode',
        )

        # L2TP
        l2tp_comparator = Comparator(
            router_option_name="wan_connection_type",
            real_option_name='conntype',
            expected_value=ConnectionType.L2TP
        )
        self.l2tp_username = SingleOption(
            name='l2tp_username',
            access_in=l2tp_comparator,
            bound_with='access_mode'
        )
        self.l2tp_pwd = SingleOption(
            name='l2tp_pwd',
            access_in=l2tp_comparator,
            bound_with='access_mode'
        )
        self.l2tp_server_ip = SingleOption(
            name='l2tp_server_ip',
            access_in=l2tp_comparator,
            bound_with='access_mode'
        )
        self.l2tp_server_name = SingleOption(
            name='l2tp_server_name',
            access_in=l2tp_comparator,
            bound_with='access_mode'
        )
        self.l2tp_type = SingleOption(
            name='l2tp_type',
            access_in=l2tp_comparator,
            bound_with='access_mode',
            validator=lambda type_: ConnectionIPType(str(type_)),
            without_load=True,
            value=ConnectionIPType.DYNAMIC_IP
        )
        self.l2tp_connect_mode = SingleOption(
            name='l2tp_connect_mode',
            access_in=l2tp_comparator,
            bound_with='access_mode',
            validator=lambda mode: ConnectTypeMode(str(mode))
        )
        self.l2tp_demand_time = SingleOption(
            name='l2tp_time',
            access_in=Comparator('l2tp_connect_mode', ConnectTypeMode.ON_DEMAND),
            bound_with='access_mode'
        )
        self.l2tp_mtu = SingleOption(
            name='l2tp_mtu',
            access_in=l2tp_comparator,
            bound_with='access_mode',
        )

        # LAN
        self.lan_mac = ReadOnlySingleOption(name='lan_mac')
        self.lan_ip = SingleOption(
            name='lan_ip',
            additional_fields={
                "rebot": '1',
                "rebot_set": '1',
                "lan_ip_set": "1"
            }
        )
        self.lan_mask = SingleOption(
            name='lan_mask',
            bound_with='lan_ip'
        )

        # DHCP server configuration
        self.dhcp_enable = SingleOption(
            name='dhcp_enable',
            validator=lambda mode: Mode(str(mode)),
            additional_fields={'dhcp_server_set': '1'}
        )
        self.dhcp_ip_range_start = SingleOption(
            name='dhcp_start_ip',
            access_in=Comparator('dhcp_enable', Mode.ENABLED),
            bound_with='dhcp_enable',
            validator=valid_ip,
        )
        self.dhcp_ip_range_end = SingleOption(
            name='dhcp_end_ip',
            access_in=Comparator('dhcp_enable', Mode.ENABLED),
            bound_with='dhcp_enable',
            validator=valid_ip,
        )

        self.reservation_list = ListOption(
            name='reservation_list',
            list_item_class=ReservationListItem,
            additional_fields={"reserve_address_set": "1"}
        )

        self.dhcp_client_list = ReadOnlyListOption(
            name='dhcp_client_list',
            list_item_class=DHCPClientListItem
        )

        # IPTV

        self.iptv_mode = SingleOption(
            name='iptv_mode',
            validator=lambda mode: IPTVMode(str(mode)),
            additional_fields={'iptv_type_set': "1"}
        )

        # Bridge section
        self.iptv_port = SingleOption(
            name='iptv_port',
            access_in=Comparator('iptv_mode', IPTVMode.BRIDGE),
            validator=lambda port: IPTVPort(str(port)),
            bound_with='iptv_mode',
            additional_fields={"rebot": '1', 'rebot_set': '1'}
        )

        # 802 Q Tag section

        q_tag_comparator = Comparator('iptv_mode', IPTVMode.Q_802_TAG_VLAN)

        # VLAN Tag
        self.iptv_internet_vlan_tag = SingleOption(
            name='vlan_tag_enable',
            access_in=q_tag_comparator,
            validator=lambda mode: Mode(str(mode)),
            bound_with='iptv_mode',
        )
        vlan_tag_comparator = Comparator(
            router_option_name='iptv_internet_vlan_tag',
            real_option_name='vlan_tag_enable',
            expected_value=Mode.ENABLED
        )
        id_validator = lambda id_: int(id_)
        priority_validator = lambda priority: IPTVPriority(str(priority))

        self.iptv_vlan_internet_id = SingleOption(
            name='vlan_id_inter',
            access_in=vlan_tag_comparator,
            validator=id_validator,
            bound_with='iptv_mode'
        )
        self.iptv_internet_vlan_priority = SingleOption(
            name='vlan_pri_inter',
            access_in=vlan_tag_comparator,
            validator=priority_validator,
            bound_with='iptv_mode'
        )
        self.iptv_vlan_id = SingleOption(
            name='vlan_id_iptv',
            access_in=vlan_tag_comparator,
            validator=id_validator,
            bound_with='iptv_mode'
        )
        self.iptv_vlan_priority = SingleOption(
            name='vlan_pri_iptv',
            access_in=vlan_tag_comparator,
            validator=priority_validator,
            bound_with='iptv_mode'
        )
        self.iptv_multi_vlan_id = SingleOption(
            name='mvlan_id_iptv',
            access_in=vlan_tag_comparator,
            validator=id_validator,
            bound_with='iptv_mode'
        )
        self.iptv_multi_vlan_priority = SingleOption(
            name='mvlan_pri_iptv',
            access_in=vlan_tag_comparator,
            validator=priority_validator,
            bound_with='iptv_mode'
        )
        self.iptv_vlan_phone_id = SingleOption(
            name='vlan_id_phone',
            access_in=vlan_tag_comparator,
            validator=id_validator,
            bound_with='iptv_mode'
        )
        self.iptv_vlan_phone_priority = SingleOption(
            name='vlan_pri_phone',
            access_in=vlan_tag_comparator,
            validator=priority_validator,
            bound_with='iptv_mode'
        )

        # IPTV port modes
        port_mode_validator = lambda mode: IPTVPortMode(str(mode))

        self.iptv_lan1_mode = SingleOption(
            name='iptv_lan1_mode',
            access_in=q_tag_comparator,
            validator=port_mode_validator,
            bound_with='iptv_mode'
        )
        self.iptv_lan2_mode = SingleOption(
            name='iptv_lan2_mode',
            access_in=q_tag_comparator,
            validator=port_mode_validator,
            bound_with='iptv_mode'
        )
        self.iptv_lan3_mode = SingleOption(
            name='iptv_lan3_mode',
            access_in=q_tag_comparator,
            validator=port_mode_validator,
            bound_with='iptv_mode'
        )
        self.iptv_lan4_mode = SingleOption(
            name='iptv_lan4_mode',
            access_in=q_tag_comparator,
            validator=port_mode_validator,
            bound_with='iptv_mode'
        )

        # Wireless settings

        self.wireless_24g_enable = SingleOption(
            name='wl_enable',
            validator=lambda mode: Mode(str(mode)),
        )

        wireless_settings_comparator = Comparator(
            router_option_name='wireless_24g_enable',
            real_option_name='wl_enable',
            expected_value=Mode.ENABLED
        )

        default_radio_fields = {
            "wl_base_set": "ap",
            "wl_sec_set": "ap",
            "wds_set": "ap"
        }

        fields_with_repeater = {
            **default_radio_fields,
            "wl_sec_set": 'repeater',
            "wl_sec_rp_set": "ap"
        }

        radio_dependent_fields = {
            'default': default_radio_fields,
            WirelessRadioMode.AP_WDS: fields_with_repeater,
            WirelessRadioMode.WDS: fields_with_repeater,
            WirelessRadioMode.REPEATER: fields_with_repeater
        }

        self.wireless_24g_radio_mode = SingleOption(
            name='net_mode',
            access_in=wireless_settings_comparator,
            validator=lambda mode: WirelessRadioMode(str(mode)),
            bound_with='wl_enable',
            dependent_fields=radio_dependent_fields
        )
        self.wireless_24g_mac = ReadOnlySingleOption(name='wl_mac')

        # Access point menu

        wireless_access_menu_comparator = ListComparator(
            router_option_name='wireless_24g_radio_mode',
            real_option_name='net_mode',
            expected_values=[WirelessRadioMode.ACCESS_POINT, WirelessRadioMode.AP_WDS]
        )

        self.wireless_24g_ssid = SingleOption(
            name='ssid',
            access_in=wireless_access_menu_comparator,
            validator=lambda ssid: str(ssid),
            bound_with='wl_enable'
        )
        self.wireless_24g_radio_band = SingleOption(
            name='wl_stand',
            access_in=wireless_access_menu_comparator,
            validator=lambda band: WirelessRadioBand(str(band)),
            bound_with='wl_enable'
        )
        self.wireless_24g_ssid_broad = SingleOption(
            name='ssid_broad',
            access_in=wireless_access_menu_comparator,
            validator=lambda mode: Mode(str(mode)),
            bound_with='wl_enable'
        )
        self.wireless_24g_region = SingleOption(
            name='region',
            access_in=wireless_access_menu_comparator,
            validator=lambda region: WirelessRegion(str(region)),
            bound_with='wl_enable'
        )
        self.wireless_24g_channel = SingleOption(
            name='channel',
            access_in=wireless_access_menu_comparator,
            validator=lambda channel: WirelessChannel(str(channel)),
            bound_with='wl_enable',
        )
        self.wireless_24g_channel_width = SingleOption(
            name="channel_width",
            access_in=wireless_access_menu_comparator,
            validator=lambda width: WirelessChannelWidth(str(width)),
            bound_with='wl_enable',
        )

        self.wireless_24g_sideband = SingleOption(
            name='channel_bind',
            access_in=ListComparator(
                router_option_name='wireless_24g_channel_width',
                expected_values=[WirelessChannelWidth.MHZ_20_40, WirelessChannelWidth.MHZ_40]
            ),
            validator=lambda band: WirelessSideBand(str(band)),
            bound_with='wl_enable'
        )

        # WDS options

        wds_comparator = ListComparator(
            router_option_name='wireless_24g_radio_mode',
            real_option_name='net_mode',
            expected_values=[WirelessRadioMode.AP_WDS, WirelessRadioMode.WDS]
        )

        self.wireless_24g_repeater_ssid = SingleOption(
            name='repeater_ssid',
            access_in=wds_comparator,
            bound_with='wl_enable',
            without_load=True,
            value=""
        )

        # Security submenu

        self.wireless_24g_security_mode = SingleOption(
            name='sec_mode',
            access_in=wireless_access_menu_comparator,
            bound_with='wl_enable',
            validator=lambda mode: SecurityType(str(mode))
        )

    def _init_options(self) -> None:
        """
        Pass this router to all option object
        and bound this options with each other
        :return:
        """
        for option_object in self.list_options + self.single_options:
            option_object._router = self

            if not self.is_readonly(option_object):
                option_object._bound()

    def __setattr__(self, key, value):
        is_list = self.is_list_option(value)
        is_single = self.is_single_option(value)

        if is_single or is_list:
            value._router = self

        if is_single:
            self._single_options.append(value)

        elif is_list:
            self._list_options.append(value)

        object.__setattr__(self, key, value)

    def update(self) -> None:
        """
        Takes a data from router and load this to all options objects
        :return:
        """
        response = self.get_data()
        valid_string = response.text.replace("'", '"')
        data = json.loads(valid_string)

        all_options = self.single_options + self.list_options

        for option_object in all_options:
            option_object.from_json(data=data)

    def post(self, data: dict) -> Response:
        """
        Modification of Client `post` for auto updating after post data.
        Because not right changes do nothing on router side
        :param data:
        :return:
        """
        resp = super(Router, self).post(data=data)
        self.update()
        return resp

    @staticmethod
    def is_readonly(option_object: Type[Option]) -> bool:
        return isinstance(option_object, (ReadOnlyListOption, ReadOnlySingleOption))

    @staticmethod
    def is_single_option(option_object: Type[Option]) -> bool:
        return isinstance(option_object, (SingleOption, ReadOnlySingleOption))

    @staticmethod
    def is_list_option(option_object: Type[Option]) -> bool:
        return isinstance(option_object, (ReadOnlyListOption, ListOption))

    @property
    def single_options(self) -> List[Union[SingleOption, ReadOnlySingleOption]]:
        return self._single_options

    @property
    def list_options(self) -> List[Union[ListOption, ReadOnlyListOption]]:
        return self._list_options

    @property
    def last_updated(self) -> BoundGroup:
        """
        Returns a last updated group
        :return:
        """
        return self._cache.last_updated_group

    def cancel_last(self) -> None:
        """
        Clear cache for last modified group of options
        :return:
        """
        group = self.last_updated
        if group is None:
            return

        for option in group.options:
            option.cancel_changes()

    def apply_last(self) -> None:
        """
        Apply changes from last modified group
        :return:
        """
        group = self.last_updated
        if group is None:
            return
        self.post(group.compress())

    def cancel_all(self) -> None:
        """
        Clear all cache and set to loaded state all options
        :return:
        """
        self._cache.groups.clear()
        self._cache.last_updated_group = None
        for group in self._cache.groups.values():
            for option in group.options:
                option.cancel_changes()

    def apply_all(self) -> None:
        """
        Apply all options, which was changed
        :return:
        """
        groups_data = self._cache.compress_all()
        for group_data in groups_data:
            self.post(group_data)
