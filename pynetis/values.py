from enum import Enum, auto


class StrEnum(str, Enum):

    def __new__(cls, value, *args, **kwargs):
        if not isinstance(value, (str, auto)):
            raise TypeError(
                f"Values of StrEnums must be strings: {value!r} is a {type(value)}"
            )
        return super().__new__(cls, value, *args, **kwargs)

    def __str__(self):
        return str(self.value)

    __repr__ = __str__


class Mode(StrEnum):

    """
    Using for most of the turn based options
    """

    ENABLED = '1'
    DISABLED = '0'


class ServerProto(StrEnum):

    """
    For create or modify item of `virtual_server_list` option
    """

    UDP = '3'
    TCP = '2'
    BOTH = '1'


class WANMode(StrEnum):

    """
    Only for `wan_mode` option
    """

    WIRED = '0'
    WIRELESS = '2'


class SecurityType(StrEnum):

    """
    Uses for `repeater_security_type`
    """

    NONE = '0'
    WEP = '1'
    WPA_PSK = '2'
    WPA2_PSK = '3'
    WPA_WPA2_PSK = '4'


class KeyType(StrEnum):

    """
    Uses for `repeater_key_type`
    """

    TKIP = '1'
    AES = '2'
    TKIP_AES = '3'


class KeyModeWPA(StrEnum):

    """
    Uses for `repeater_key_mode_wpa`
    """

    HEX = "0"
    ASCII = '1'


class ConnectionType(StrEnum):

    """
    Uses for `connection_type`
    """

    STATIC_IP = '0'
    DYNAMIC_IP = '1'
    PPPOE = '3'
    UNIFI_PPPOE = '5'
    MAXIS_PPPOE = '8'
    PPTP = '7'
    L2TP = '6'


class ConnectTypeMode(StrEnum):

    """
    Uses for `pppoe_connect_mode`, `pptp_connect_mode`, `l2tp_connect_mode`
    """

    AUTOMATICALLY = '0'
    ON_DEMAND = '1'
    MANUALLY = '2'


class ConnectionIPType(StrEnum):

    """
    Uses for `pptp_type`, `l2tp_type`
    """

    DYNAMIC_IP = '0'
    STATIC_IP = '1'


class IPTVMode(StrEnum):

    """
    Uses for `iptv_mode`
    """

    AUTO = '0'
    BRIDGE = '1'
    Q_802_TAG_VLAN = '2'


class IPTVPort(StrEnum):

    """
    Uses for `iptv_port`
    """

    LAN1 = '0'
    LAN1_LAN2 = '1'


class IPTVPriority(StrEnum):

    """
    Uses for `iptv_vlan_priority`
    """

    NONE = '0'
    VERY_LOW = '1'
    LOW = '2'
    PRE_MEDIUM = '3'
    MEDIUM = '4'
    PAST_MEDIUM = '5'
    HIGH = '6'
    VERY_HIGH = '7'


class IPTVPortMode(StrEnum):

    """
    Uses for `iptv_lan1_mode`, `iptv_lan2_mode` and etc
    """

    INTERNET = '0'
    IPTV = '1'
    IP_PHONE = '2'


class WirelessRadioMode(StrEnum):

    """
    Uses for `wireless_24g_radio_mode`
    """

    ACCESS_POINT = '0'
    REPEATER = '4'
    AP_WDS = '6'
    WDS = '5'
    CLIENT = '3'


class WirelessRadioBand(StrEnum):

    """
    Uses for `wireless_24g_radio_band`
    """

    B_802_11 = '0'
    G_802_11 = '1'
    N_802_11 = '7'
    B_G_802_11 = '2'
    G_N_802_11 = '9'
    B_G_N_802_11 = '10'


class WirelessRegion(StrEnum):

    """
    Uses for `wireless_24g_region`
    """

    US = '1'
    EU = '3'
    MKK = '6'
    IC = '2'


class WirelessChannel(StrEnum):

    """
    Uses for `wireless_24g_channel`
    """

    AUTO = '0'
    CHANNEL_5 = '5'
    CHANNEL_6 = '6'
    CHANNEL_7 = '7'
    CHANNEL_8 = '8'
    CHANNEL_9 = '9'
    CHANNEL_10 = '10'
    CHANNEL_11 = '11'
    CHANNEL_12 = '12'
    CHANNEL_13 = '13'


class WirelessChannelWidth(StrEnum):

    """
    Uses for `wireless_24g_channel_width`
    """

    MHZ_20 = '0'
    MHZ_40 = '1'
    MHZ_20_40 = '2'


class WirelessSideBand(StrEnum):

    """
    Uses for `wireless_24g_sideband`
    """

    LOWER = '0'
    UPPER = '1'
